﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace InformeFomento
{
    public partial class FrmInformeFomento : Form
    {
        string FechaMM_DD_YYYY = "";
        string FechaDD_MM_YYYY = "";
        string FechaIniYYYY_MM_DD = "";
        string FechaFinYYYY_MM_DD = "";
        string FechaIniYYYYMMDD = "";
        string FechaFinYYYYMMDD = "";
        DataSet DataMySQL = new DataSet();

        public FrmInformeFomento()
        {
            InitializeComponent();
        }

        #region Funciones del Proceso

        /*
        0)	Inicializamos tablas entre fechas para no duplicar datos
        */

        private void P0LimpiaTablas()
        {
            //Limpiar VentasxtrayectoHS

            string query = "DELETE FROM [dbo].[VentasxtrayectoHS];";
            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            //Limpiar ventas_GMV
            query = "DELETE FROM [dbo].[ventas_GMV] WHERE FECHA_VENTA >= '" + FechaDD_MM_YYYY + "' AND FECHA_VENTA <= '"+ FechaDD_MM_YYYY + "';";
            command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            //limpiar registros_fomento
            query = "DELETE FROM [dbo].[registros_fomento] WHERE [fecha_viaje] >= '" + FechaDD_MM_YYYY + "' AND [fecha_viaje] <= '" + FechaDD_MM_YYYY + "';";
            command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            // limpiar registros_fomento_procesados
            query = "DELETE FROM [dbo].[registros_fomento_procesados] WHERE [fecha_viaje] >= '" + FechaDD_MM_YYYY + "' AND [fecha_viaje] <= '" + FechaDD_MM_YYYY + "';"
            command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();
        }


        /*
        1)	Ejecutar el procedimiento almacenado de GMV con HeidiSQL, y exportarlo como .csv con nombres de columnas

            Llamada a procedimiento almacenado.sql
        */

        private void P1LeeMySql()
        {
            string query = "CALL `sp_ventasportrayecto`('" + FechaIniYYYY_MM_DD + " 00:00:00','" + FechaFinYYYY_MM_DD + " 23:59:59',900,4500)"; //TODO!!!!!!!!!!!

            MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
            conn_string.Server = "172.16.10.131";
            conn_string.UserID = "uIntegra";
            conn_string.Password = "interbus2017";
            conn_string.Database = "interbus";

            using (MySqlConnection conn = new MySqlConnection(conn_string.ToString()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(query, conn))
                {
                    using (MySqlDataAdapter adap = new MySqlDataAdapter(cmd))
                    {
                        adap.Fill(DataMySQL);

                    }
                }
                DataMySQL.Tables[0].Columns[8].ColumnName = "TIPO_OPERACION_DESC";

                conn.Close();
            }

        }

        /*
        2)	Importar archivo csv exportado del Heidi en VentasxTrayectoHS(tiene el formato de columnas correcto)
        */

        private void P2ImportarDatos()
        {
            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conexion))
            {
                bulkCopy.DestinationTableName = "dbo.VentasxtrayectoHS";
                bulkCopy.WriteToServer(DataMySQL.Tables[0]);
            }
            conexion.Close();
        }

        /*
        3)	Hacer el insert de los registros anteriores en ventas_GMV
            Copiar registros a Ventas_GMV v301017.sql
        */

        private void P3CopiaVentasGMV()
        {
            string query = "INSERT INTO ventas_GMV (fecha_liquidacion, fecha_venta, codigo_linea, trayecto, origen, destino, recaudado, tipo_operacion) " +
                "SELECT CONVERT(datetime, [\"FECHA_LIQUIDACION\"],103), CONVERT(datetime,[\"FECHA_VENTA\"], 103), [\"CODIGO_LINEA\"], [\"TRAYECTO\"], " +
                "[\"ORIGEN\"], [\"DESTINO\"], [\"RECAUDADO\"], [\"TIPO_OPERACION_DESC\"] FROM VentasxtrayectoHS ";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        4)	(mete los registros en registros_fomento con procedencia = 1
            Insertar registros GMV.sql
        */

        private void P4InsertaRegistrosGmv()
        {
            string query = "INSERT INTO registros_fomento (empresa, fecha_viaje, codigo_linea, codigo_trayecto, cod_origen, cod_destino, " +
                "tipo_operacion, kms, recaudacion, procedencia) SELECT 879, FECHA_VENTA, CODIGO_LINEA, TRAYECTO, ORIGEN, DESTINO, TIPO_OPERACION, " +
                "KMS, recaudado, 1 FROM ventas_GMV WHERE FECHA_LIQUIDACION >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        5)	Cambiar destino = 0 en TSCs GMV
            Actualiza destinos 0 de TSC.sql
        */

        private void P5ActualizarDestinos()
        {
            string query =
                        "DECLARE @linea int; " +
                        "DECLARE @fechaDesde date; " +
                        "SET @fechaDesde = '" + FechaMM_DD_YYYY + "'; " +
                        "SET @linea = 956; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4109 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea = @linea " +
                        "AND codigo_trayecto % 2 = 0 AND fecha_viaje >= @fechaDesde; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4163 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea = @linea " +
                        "AND codigo_trayecto % 2 = 1 AND fecha_viaje >= @fechaDesde; " +
                        "SET @linea = 955; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4163 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea = @linea " +
                        "AND codigo_trayecto % 2 = 0 AND fecha_viaje >= @fechaDesde; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4079 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea = @linea " +
                        "AND codigo_trayecto % 2 = 1 AND fecha_viaje >= @fechaDesde; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4079 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea NOT IN(955, 956) " +
                        "AND codigo_trayecto % 2 = 0 AND fecha_viaje >= @fechaDesde; " +
                        "UPDATE registros_fomento " +
                        "SET cod_destino = 4163 " +
                        "WHERE tipo_operacion = 37 AND procedencia = 1 AND codigo_linea NOT IN(955, 956) " +
                        "AND codigo_trayecto % 2 = 1 AND fecha_viaje >= @fechaDesde;";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        6)	Completar km teóricos de origen GMV en la tabla registros_fomento
            completar registros fomento.sql
        */

        private void P6CompletarRegistrosFomento()
        {
            string query = "UPDATE registros_fomento SET kms = (SELECT TOP 1 kms FROM kilometros_origen_destino" +
                " WHERE registros_fomento.CODIGO_LINEA = cod_linea AND registros_fomento.cod_origen = cod_orig AND" +
                " registros_fomento.cod_destino = cod_dest) WHERE procedencia = 1 AND fecha_viaje >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }


        /*
        7)	Insertar registros Movelia.sql
        */

        private void P7InsertaRegistrosMovelia()
        {
            string query = "INSERT INTO registros_fomento (empresa, fecha_viaje, codigo_linea, cod_origen, cod_destino, tipo_operacion, kms, recaudacion, ca, procedencia) " +
                "SELECT 879, convert(datetime, CONVERT(CHAR(8), FECHA_VIAJE)), linea, ORIGEN, DESTINO, 1, KMS, importe_neto, ca, 0 FROM billetes_procesados " +
                "WHERE(empresa_bill = 879 AND(linea >= 50000 and linea <= 99999) AND cod_dto <> '41') AND(fecha_viaje >= " + FechaIniYYYYMMDD + " and fecha_viaje <= " + FechaFinYYYYMMDD + ")";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }


        /*
        8)	Actualizar códigos de parada _GMV en registros de Movelia(procedencia = 0) 
            update códigos parada registros fomento Movelia.sql
        */

        private void P8UpdateCodigosParadaMovelia()
        {
            string query = "UPDATE registros_fomento SET cod_origen_gmv = (SELECT TOP 1 cod_monetica FROM paradas WHERE cod_movelia_simpl = cod_origen)," +
                " cod_destino_gmv = (SELECT TOP 1 cod_monetica FROM paradas WHERE cod_movelia_simpl = cod_destino) WHERE procedencia = 0";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }


        /*
        9)	Actualizar códigos de parada _GMV en registros de GMV(procedencia = 1) 
            update códigos parada registros fomento GMV.sql
        */

        private void P9UpdateCodigosParadaGMV()
        {
            string query = "UPDATE registros_fomento SET cod_origen_gmv = cod_origen, cod_destino_gmv = cod_destino WHERE procedencia = 1";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        10)	Eliminar registros que no hay que exportar por paradas que no se deben hacer
            Eliminar paradas que no se deben exportar.sql
        */

        private void P10EliminarParadas()
        {
            string query = "DELETE FROM registros_fomento WHERE(cod_origen_gmv IN(4202, 4261, 4222) OR cod_destino_gmv IN(4202, 4261, 4222))" +
                " AND codigo_linea<>959 AND fecha_viaje >= '" + FechaMM_DD_YYYY + "' AND procedencia = 1";
            string query2 = "DELETE FROM registros_fomento WHERE(cod_origen_gmv IN(4202, 4261, 4222) OR cod_destino_gmv IN(4202, 4261, 4222)) " +
                "AND codigo_linea NOT IN(51346, 51347) AND fecha_viaje >= '" + FechaMM_DD_YYYY + "' AND procedencia = 0";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            SqlCommand command2 = new SqlCommand(query2, conexion);
            command2.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        12)	Copiado a registros procesados
            copia de registros a procesados.sql
        */

        private void P11CopiaAProcesados()
        {
            string query = "INSERT INTO registros_fomento_procesados(empresa, fecha_viaje, codigo_linea, codigo_trayecto, cod_origen," +
                " cod_destino, tipo_operacion, kms,	 recaudacion, procedencia, ca, cod_origen_gmv, cod_destino_gmv) " +
                "SELECT* FROM registros_fomento WHERE fecha_viaje >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        13)	Actualiza cod_trayecto en registros Movelia.sql
        */

        private void P12ActualizaTraectosMovelia()
        {
            string query = "UPDATE registros_fomento_procesados SET codigo_trayecto = SUBSTRING(codigo_linea, 2, 4)" +
                " where procedencia = 0 AND fecha_viaje >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        14)	Actualizar campo IV(Actualiza iv trayectos fomento.sql) (versiones de 09052017)
        */

        private void P13ActualizaCampoIV()
        {
            string query = "UPDATE registros_fomento_procesados SET IV = (select iv FROM kilometros_origen_destino " +
                "WHERE kilometros_origen_destino.cod_tray_gmv = registros_fomento_procesados.codigo_trayecto" +
                " AND kilometros_origen_destino.cod_orig = cod_origen_gmv AND kilometros_origen_destino.cod_dest = cod_destino_gmv)" +
                " WHERE fecha_viaje >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        15)	Invierte códigos fomento vueltas v09052017.sql
        */

        private void P14InvierteCodigosFomento()
        {
            string query = "UPDATE registros_fomento_procesados SET cod_origen_fomento = CASE iv WHEN 1 THEN cod_origen_gmv" +
                " ELSE cod_destino_gmv END  ,cod_destino_fomento = CASE iv WHEN 1 THEN cod_destino_gmv ELSE cod_origen_gmv END" +
                " from registros_fomento_procesados WHERE fecha_viaje >= '" + FechaMM_DD_YYYY + "'";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        /*
        16)	Invertir kilómetros en cancelaciones.sql
        */

        private void P15InvierteKmCancelaciones()
        {
            string query = "UPDATE registros_fomento_procesados SET kms = kms * -1 WHERE ca = 1 AND fecha_viaje >= '" + FechaMM_DD_YYYY + "' ";

            SqlConnection conexion = new SqlConnection(Properties.Settings.Default.conexionSQL);
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);
            command.ExecuteNonQuery();

            conexion.Close();
        }

        #endregion

        #region Funciones BackgroundWorker

        private void bw1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            e.Result = ProcesaDatos(worker, e);
        }

        private string ProcesaDatos(BackgroundWorker worker, DoWorkEventArgs e)
        {
            worker.ReportProgress(0, "Iniciando (" + DateTime.Now.ToString("HH:mm:ss") + ")");

            P0LimpiaTablas();

            P1LeeMySql();
            worker.ReportProgress(1, "1/15 Procedimiento Mysql Completado");

            P2ImportarDatos();
            worker.ReportProgress(2, "2/15 Importa datos de Mysql Completado");

            P3CopiaVentasGMV();
            worker.ReportProgress(3, "3/15 Copia Ventas GMV Completado");

            P4InsertaRegistrosGmv();
            worker.ReportProgress(4, "4/15 Inserta Registros Gmv Completado");

            P5ActualizarDestinos();
            worker.ReportProgress(5, "5/15 Actualizar Destinos Completado");

            P6CompletarRegistrosFomento();
            worker.ReportProgress(6, "6/15 Completar Registros Fomento Completado");

            P7InsertaRegistrosMovelia();
            worker.ReportProgress(7, "7/15 Inserta Registros Movelia Completado");

            P8UpdateCodigosParadaMovelia();
            worker.ReportProgress(8, "8/15 Update Codigos Parada Movelia Completado");

            P9UpdateCodigosParadaGMV();
            worker.ReportProgress(9, "9/15 Update Codigos Parada GMV Completado");

            P10EliminarParadas();
            worker.ReportProgress(10, "10/15 Eliminar Paradas Completado");

            P11CopiaAProcesados();
            worker.ReportProgress(11, "11/15 Copia A Procesados Completado");

            P12ActualizaTraectosMovelia();
            worker.ReportProgress(12, "12/15 Actualiza Traectos Movelia Completado");

            P13ActualizaCampoIV();
            worker.ReportProgress(13, "13/15 Actualiza Campo IV Completado");

            P14InvierteCodigosFomento();
            worker.ReportProgress(14, "14/15 Invierte Codigos Fomento Completado");

            P15InvierteKmCancelaciones();
            worker.ReportProgress(15, "15/15 Invierte Km Cancelaciones Completado");

            return "Completado Correctamente";
        }

        private void bw1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbProgreso.Value = e.ProgressPercentage;
            tbMensajesSistema.AppendText(e.UserState != null ? e.UserState.ToString() + Environment.NewLine : "");

        }

        private void bw1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                tbMensajesSistema.AppendText(e.Error.Message);
            }
            else
            {
                tbMensajesSistema.AppendText("Proceso terminado");
            }

            tbMensajesSistema.AppendText(" (" + DateTime.Now.ToString("HH:mm:ss") + ")");

            EnableControls();
        }

        #endregion

        #region Interfaz

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            // Recogemos la fecha
            FechaMM_DD_YYYY = dtDesde.Value.ToString("MM-dd-yyyy");
            FechaDD_MM_YYYY = dtDesde.Value.ToString("dd-MM-yyyy");
            FechaIniYYYY_MM_DD = dtDesde.Value.ToString("yyyy-MM-dd");
            FechaFinYYYY_MM_DD = dtHasta.Value.ToString("yyyy-MM-dd");
            FechaIniYYYYMMDD = dtDesde.Value.ToString("yyyyMMdd");
            FechaFinYYYYMMDD = dtHasta.Value.ToString("yyyyMMdd");

            //Iniciamos Proceso
            DisableControls();

            tbMensajesSistema.Text = String.Empty;
            pbProgreso.Value = 0;

            bw1.RunWorkerAsync();
        }

        private void DisableControls()
        {
            dtDesde.Enabled = false;
            dtHasta.Enabled = false;
            btnProcesar.Enabled = false;
        }

        private void EnableControls()
        {
            dtDesde.Enabled = true;
            dtHasta.Enabled = true;
            btnProcesar.Enabled = true;
        }

        #endregion

    }
}
